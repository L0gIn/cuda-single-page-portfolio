// Requis
var gulp = require('gulp');

// Include plugins
var plugins = require('gulp-load-plugins')(); // Load all plugins from package.json

// Path files
var srcCssPath = './assets/src/css';
var distCssPath = './assets/dist/css';

// "build_css" task = autoprefixer + CSScomb + beautify (source -> destination)
var buildCssTask = function () {
    return gulp.src(srcCssPath + '/*.css')
        .pipe(plugins.cached('building_css'))
        .pipe(plugins.csscomb())
        .pipe(plugins.cssbeautify({indent: '    '}))
        .pipe(plugins.autoprefixer())
        .pipe(gulp.dest(srcCssPath + '/'));
};

// "minify_css" task = Minify CSS + rename to *.min.css (destination -> destination)
var minifyCssTask = function (src, dest) {
    return gulp.src(src)
        .pipe(plugins.cached('minifying_css'))
        .pipe(plugins.csso())
        .pipe(plugins.rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(dest));
};

//Task function handling
gulp.task('build_css' , buildCssTask);
gulp.task('minify_css', function() {
    return minifyCssTask(srcCssPath + '/*.css', distCssPath + '/')
});

//Task names config
gulp.task('build'   , ['build_css']);
gulp.task('prod'    , ['build_css', 'minify_css']);
gulp.task('default' , ['build']);

// Watch task
gulp.task('watch', function () {
    gulp.watch(srcCssPath + '/*.css', ['build']);
});
