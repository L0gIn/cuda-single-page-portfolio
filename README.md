# Cuda Single Page Portfolio #

Cuda Single Page Portfolio is a simple HTML5 render from the PSD included in *design/* folder.

### What is included in this repository? ###

* PSD File
* HTML5 file with CSS and IMG assets
* Use of [Bootstrap 3](http://getbootstrap.com/)
* Use of [Fontawesome 4](https://fortawesome.github.io/Font-Awesome/)
* Use of [Gulp](http://gulpjs.com/)